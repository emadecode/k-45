import React, { FC } from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import Dashboard from './areas/Dashboard';
import AuthPage from './pages/Auth.page';
import { AuthGuard } from './core/guards/Auth.guard';
import LogoutPage from './pages/Logout.page';
import Loading from './components/Loading';
import { IAppState } from './redux/reducers/index';
import { connect } from 'react-redux';

interface IPropsModel {
  modifiedTime: number
}
const App: FC<IPropsModel> = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/auth" component={AuthPage} />
        <Route exact path="/logout" component={LogoutPage} />
        <AuthGuard path="/" component={Dashboard} />
      </Switch>
      <ToastContainer />
      <Loading />
    </BrowserRouter>
  );
}
const mapStateToProps = (state: IAppState) => {
  return { modifiedTime: state.general.rootModifiedTime }
}
export default connect(mapStateToProps)(App);

import React, { FC, useEffect } from 'react'
import { DragDropContext } from 'react-beautiful-dnd'

import Breadcrumbs from '../components/Breadcrumbs';
import { IMoveTodoItemModel, ITodoListModel } from '../core/models/todo/todo.models';
import todoService from './../core/services/todo.service';
import { IAppState } from './../redux/reducers/index';
import { connect } from 'react-redux';
import TodoList from '../components/TodoList';
import AddListForm from '../forms/AddList.form';
import todoActionCreators from './../redux/actions/todo.action';

interface IPropsModel {
    todoLists: Array<ITodoListModel>
}

const TodoListPage: FC<IPropsModel> = ({ todoLists }) => {

    useEffect(() => {
        todoService.getTodosListRequest();
    }, [])

    const renderedTodoLists = todoLists.map(item => <TodoList key={item.id} todoList={item} />)

    const onDragEnd = result => {
        const { draggableId, source, destination } = result;

        // dropped outside the list
        if (!destination) {
            return;
        }

        if (source.droppableId !== destination.droppableId) {

            const model: IMoveTodoItemModel = {
                id: Number(draggableId),
                listId :  Number(destination.droppableId)
            }

            todoActionCreators.moveTodoItem(model)
        }
    };


    return (
        <>
            <Breadcrumbs title="Todo List" subtitle="مدیریت لیست Todo" isRounded={true} link="/">
            </Breadcrumbs>
            <section className="todo-list-section">
                <div className="container">
                    <div className="d-flex flex-wrap align-items-start">
                        <DragDropContext onDragEnd={onDragEnd}>

                            {renderedTodoLists}
                        </DragDropContext>

                        <div className="todo-list">
                            <div className="todo-list__header">
                                <h4 className="todo-list__title">افزودن لیست جدید</h4>
                            </div>
                            <div className="todo-list__body">
                                <AddListForm />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

const mapStatesToProps = (state: IAppState) => {
    return {
        todoLists: state.todo.todoLists
    }
}
export default connect(mapStatesToProps)(TodoListPage);
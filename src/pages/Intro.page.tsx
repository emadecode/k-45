import React from 'react'
import { Link } from 'react-router-dom'
import { CategoryAlt } from '@styled-icons/boxicons-solid/CategoryAlt'

import Breadcrumbs from '../components/Breadcrumbs'
import authService from '../core/services/auth.service'

const IntroPage = () => {

    return (
        <>
            <Breadcrumbs title="پنل کاربری" subtitle="پنل کاربری شما" isRounded={true} >
            </Breadcrumbs>
            <section className="section">
                <div className="container">
                    <div className="intro-content">
                        <h3 className="intro-message">خوش آمدید <span className="intro-message__name">{authService.getAuthName()}</span> عزیز</h3>
                        <Link className="gi-btn intro-button" to="/todo-list">
                            <CategoryAlt className="mobile-navbar__icon" /><span>مدیریت لیست کارها</span>
                        </Link>
                    </div>
                </div>
            </section>
        </>
    )
}


export default IntroPage;
import { useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import authService from '../core/services/auth.service';

const LogoutPage = () => {
    useEffect(() => {
        authService.logout();
    })
    return <Redirect to="/auth" />;
}

export default LogoutPage
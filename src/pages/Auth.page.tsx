import React from 'react';
import { connect } from "react-redux";

import introImage from './../assets/images/banners/register.png';
import LoginForm from '../forms/Login.form';


const AuthPage = () => {

    return (
        <section className="auth-page">
            <div className="auth-page__right">
                <div className="auth-page__wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-xl-8 offset-xl-2">
                                <div className="auth-content">
                                    <h1 className="auth-content__title">صفحه ورود کاربر</h1>
                                    <img src={introImage} className="auth-content__image" alt="intro" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="auth-page__left">
                <div className="auth-page__wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                                <LoginForm />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = state => {
    return { auth: state.auth }
}

export default connect(mapStateToProps)(AuthPage);
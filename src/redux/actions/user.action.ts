import appStore from "./../store";
import { userActionTypes } from "../actionTypes";
import { IUserModel } from "../../core/models/user/user.models";

class UserActionCreators {
    addUser = (data: IUserModel | undefined) => {
        appStore.dispatch({ type: userActionTypes.ADD_USER, data })
    }
}
const userActionCreators = new UserActionCreators();
export default userActionCreators;




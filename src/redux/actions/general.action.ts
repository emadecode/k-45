import appStore from "./../store";
import { generalActionTypes } from "../actionTypes";

class GeneralActionCreator {
    reRenderRootElement() {
        appStore.dispatch({ type: generalActionTypes.RE_RENDER_ROOT})
    }
    setLoadingStatus(data: boolean) {
        appStore.dispatch({ type: generalActionTypes.SET_LOADING_STATUS, data })
    }
    setMobileMenuStatus(isMobileMenuOpen: boolean) {
        appStore.dispatch({ type: generalActionTypes.SET_MOBILE_MENU_STATUS, data: isMobileMenuOpen })
    }
    loadState() {
        appStore.dispatch({ type: generalActionTypes.LOAD_STATE })
    }
    userLogout() {
        appStore.dispatch({ type: generalActionTypes.USER_LOGOUT })
    }
}
const generalActionCreator = new GeneralActionCreator();
export default generalActionCreator;


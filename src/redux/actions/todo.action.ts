import appStore from "./../store";
import { todoActionTypes } from "../actionTypes";
import { IAddTodoItemModel, IAddTodoListModel, IMoveTodoItemModel, ITodoItemModel, IUpdateTodoItemModel } from './../../core/models/todo/todo.models';

class TodoActionCreators {
    setTodoItems = (data: Array<ITodoItemModel>) => {
        appStore.dispatch({ type: todoActionTypes.SET_TODO_ITEMS, data })
    }
    addTodoItem = (data: IAddTodoItemModel) => {
        appStore.dispatch({ type: todoActionTypes.ADD_TODO_ITEM, data })
    }
    updateTodoItem = (data: IUpdateTodoItemModel) => {
        appStore.dispatch({ type: todoActionTypes.UPDATE_TODO_ITEM, data })
    }
    moveTodoItem = (data: IMoveTodoItemModel) => {
        appStore.dispatch({ type: todoActionTypes.MOVE_TODO_ITEM, data })
    }
    deleteTodoItem = (todoId: number) => {
        appStore.dispatch({ type: todoActionTypes.DELETE_TODO_ITEM, todoId })
    }
    addTodoList = (data: IAddTodoListModel) => {
        appStore.dispatch({ type: todoActionTypes.ADD_TODO_LIST, data })
    }
    updateTodoList = (data: IAddTodoListModel) => {
        appStore.dispatch({ type: todoActionTypes.UPDATE_TODO_LIST, data })
    }
    deleteTodoList = (listId: number) => {
        appStore.dispatch({ type: todoActionTypes.DELETE_TODO_LIST, listId })
    }
}

const todoActionCreators = new TodoActionCreators();
export default todoActionCreators;




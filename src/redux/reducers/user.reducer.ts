
import { userActionTypes } from '../actionTypes';
import { IUserModel } from './../../core/models/user/user.models';

export interface IUserState {
    usersList: Array<IUserModel> | undefined
}

const User_INITIAL_STATE: IUserState = {
    usersList: []
}

const userReducerHelper = {
    addUser: (state: IUserState, data: IUserModel) => {
        return { ...state, usersList: [ ...state.usersList, data ] };
    }
}

const userReducer = (state : IUserState = User_INITIAL_STATE, action : any) => {
    switch (action.type) {
        case userActionTypes.ADD_USER:
            return userReducerHelper.addUser(state, action.data);

        default: return state;
    }
}

export default userReducer;
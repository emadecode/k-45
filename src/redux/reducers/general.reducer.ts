import { generalActionTypes } from '../actionTypes';

export interface IGeneralState {
    rootModifiedTime: number
    showLoading: boolean,
    isMobileMenuOpen: boolean,
}

const GENERAL_INITIAL_STATE: IGeneralState = {
    rootModifiedTime: new Date().getTime(),
    showLoading: false,
    isMobileMenuOpen: false,
}

const generalReducerHelper = {
    reRenderRootElement: (state: IGeneralState) => {
        return { ...state, rootModifiedTime: new Date().getTime() };
    },
    setLoadingStatus: (state: IGeneralState, data: boolean) => {
        return { ...state, showLoading: data };
    },
    setMobileMenuStatus: (state: IGeneralState, data: boolean) => {
        return { ...state, isMobileMenuOpen: data };
    }
}

const generalReducer = (state: IGeneralState = GENERAL_INITIAL_STATE, action: any) => {
    switch (action.type) {
        case generalActionTypes.RE_RENDER_ROOT:
            return generalReducerHelper.reRenderRootElement(state);
        case generalActionTypes.SET_LOADING_STATUS:
            return generalReducerHelper.setLoadingStatus(state, action.data);
        case generalActionTypes.SET_MOBILE_MENU_STATUS:
            return generalReducerHelper.setMobileMenuStatus(state, action.data);
        default: return state;
    }
}

export default generalReducer;

import { IAddTodoItemModel, IMoveTodoItemModel, ITodoItemModel, ITodoListModel, IUpdateTodoListModel } from '../../core/models/todo/todo.models';
import { todoActionTypes } from '../actionTypes';
import { IAddTodoListModel, IUpdateTodoItemModel } from './../../core/models/todo/todo.models';

const InitialTodoList: ITodoListModel = {
    id: 1,
    title: "Todo Typicode",
    description: "default todo list",
    createdAt: new Date().getTime(),
    updateAt: new Date().getTime(),

}

export interface ITodoState {
    todoItems: Array<ITodoItemModel> | undefined,
    todoLists: Array<ITodoListModel>
}

const TODO_INITIAL_STATE: ITodoState = {
    todoItems: undefined,
    todoLists: [InitialTodoList]
}

const todoReducerHelper = {
    setTodoItems: (state: ITodoState, data: Array<ITodoItemModel>) => {
        return { ...state, todoItems: [...data] };
    },
    addTodoItem: (state: ITodoState, data: IAddTodoItemModel) => {
        const todoItemIds = state.todoItems.length ? state.todoItems.map(item => item.id) : [0];

        var lastId = todoItemIds.reduce(function (a, b) {
            return Math.max(a, b);
        });
        const debouncedItem: ITodoItemModel = { ...data, id: lastId + 1 };

        return { ...state, todoItems: [...state.todoItems, { ...debouncedItem }] };
    },
    updateTodoItem: (state: ITodoState, data: IUpdateTodoItemModel) => {
        const todoItems = state.todoItems.map(item => item.id === data.id ? data : item);
        return { ...state, todoItems: todoItems };
    },
    moveTodoItem: (state: ITodoState, data: IMoveTodoItemModel) => {
        const todoItems = state.todoItems.map(item => item.id === data.id ? { ...item, listId: data.listId } : item);
        return { ...state, todoItems: todoItems };
    },
    deleteTodoItem: (state: ITodoState, todoId: number) => {
        const newTodos = state.todoItems.filter(item => item.id !== todoId);

        return { ...state, todoItems: newTodos };
    },
    addTodoList: (state: ITodoState, data: IAddTodoListModel) => {
        const todoListIds = state.todoLists.length ? state.todoLists.map(item => item.id) : [0];
        var lastId = todoListIds.reduce(function (a, b) {
            return Math.max(a, b);
        });
        const debouncedList: ITodoListModel = { ...data, id: lastId + 1 };

        return { ...state, todoLists: [...state.todoLists, { ...debouncedList }] };
    },
    updateTodoList: (state: ITodoState, data: IUpdateTodoListModel) => {
        const todoLists = state.todoLists.map(item => item.id === data.id ? data : item);
        return { ...state, todoLists: todoLists };

    },
    deleteTodoList: (state: ITodoState, listId: number) => {

        const newList = state.todoLists.filter(item => item.id !== listId);
        const newTodos = state.todoItems.filter(item => item.listId !== listId);

        return { ...state, todoLists: newList, todoItems: newTodos };
    }
}

const todoReducer = (state: ITodoState = TODO_INITIAL_STATE, action: any) => {
    switch (action.type) {
        case todoActionTypes.SET_TODO_ITEMS:
            return todoReducerHelper.setTodoItems(state, action.data);
        case todoActionTypes.ADD_TODO_ITEM:
            return todoReducerHelper.addTodoItem(state, action.data);
        case todoActionTypes.UPDATE_TODO_ITEM:
            return todoReducerHelper.updateTodoItem(state, action.data);
        case todoActionTypes.MOVE_TODO_ITEM:
            return todoReducerHelper.moveTodoItem(state, action.data);
        case todoActionTypes.DELETE_TODO_ITEM:
            return todoReducerHelper.deleteTodoItem(state, action.todoId);
        case todoActionTypes.ADD_TODO_LIST:
            return todoReducerHelper.addTodoList(state, action.data);
        case todoActionTypes.UPDATE_TODO_LIST:
            return todoReducerHelper.updateTodoList(state, action.data);
        case todoActionTypes.DELETE_TODO_LIST:
            return todoReducerHelper.deleteTodoList(state, action.listId);

        default: return state;
    }
}

export default todoReducer;
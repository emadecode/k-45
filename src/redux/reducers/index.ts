import { combineReducers } from 'redux';
import generalReducer, { IGeneralState } from './general.reducer';
import userReducer, { IUserState } from './user.reducer';
import { generalActionTypes } from '../actionTypes';
import todoReducer, { ITodoState } from './todo.reducer';
import userService from '../../core/services/user.service';

export interface IAppState {
    todo: ITodoState,
    general: IGeneralState,
    user: IUserState
}

export const appReducer = combineReducers({
    todo: todoReducer,
    general: generalReducer,
    user: userReducer
});

export const rootReducer = (state, action) => {
    if (action.type === generalActionTypes.USER_LOGOUT) {
        state = undefined
    }
    if (action.type === generalActionTypes.LOAD_STATE) {
        state = userService.loadUsersData();
    }
    return appReducer(state, action);
}
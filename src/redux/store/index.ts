import { createStore } from "redux";
import userService from "../../core/services/user.service";
import { rootReducer } from "./../reducers/index";

const appStore = createStore(rootReducer);

appStore.subscribe(() => {
    userService.saveUserData(appStore.getState());
});

export default appStore;
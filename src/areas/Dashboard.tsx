import React, { Fragment, Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import Header from '../layout/Header'
import MobileMenu from '../components/mobile/MobileMenu'
import IntroPage from './../pages/Intro.page';
import TodoListPage from './../pages/TodoList.page';
import generalActionCreator from './../redux/actions/general.action';


class Dashboard extends Component {
    componentDidMount(){
        generalActionCreator.loadState();
    }
    render() {
        return (
            <Fragment>
                <Header />
                <MobileMenu />
                <Switch>
                    <Route exact path="/" component={IntroPage} />
                    <Route exact path="/todo-list" component={TodoListPage} />
                    <Redirect from="*" to="/" />
                </Switch>
            </Fragment>
        );
    }
}

export default Dashboard;
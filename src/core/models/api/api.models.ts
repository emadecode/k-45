export interface IApiCallbackModel<> {
    onSuccess?: (data: any) => void,
    onFail?: (data: any) => void
    onError?: (data: any) => void
    onFinally?: () => void
}

export interface IApiResultModel<T> {
    status: boolean,
    message: string,
    body: T
}

export interface IAuthCodeDataResultModel {
    codeExpireTime: string,
    codeGenerateTime: string,
    username: string
}

export interface IAuthTokenDataResultModel {
    firstName: string,
    lastName: string,
    token: string,
    username: string
}
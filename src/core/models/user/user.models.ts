export interface ICurrentUserModel {
    username: string
}
export interface IUserModel {
    id: number,
    name: string,
}
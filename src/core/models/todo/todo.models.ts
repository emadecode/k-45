export interface ITodoItemModel {
    completed: boolean,
    id: number,
    title: string,
    userId: number | null,
    listId: number,
}
export interface IAddTodoItemModel {
    completed: boolean,
    title: string,
    userId: number | null,
    listId: number,
}
export interface IUpdateTodoItemModel {
    id: number,
    completed: boolean,
    title: string,
    userId: number | null,
    listId: number,
}
export interface IMoveTodoItemModel {
    id: number,
    listId: number,
}
export interface ITodoListModel {
    id: number,
    title: string,
    description: string,
    createdAt: number,
    updateAt: number
}
export interface IAddTodoListModel{
    title: string,
    description: string,
    createdAt: number,
    updateAt: number
}
export interface IUpdateTodoListModel{
    id: number,
    title: string,
    description: string,
    createdAt: number,
    updateAt: number
}
import axios from 'axios';
import generalActionCreator from '../../redux/actions/general.action';
import authService from '../services/auth.service';
import messageService from './../services/message.service';

const axiosApi = axios.create({
    baseURL: process.env.REACT_APP_BASE_API,
    headers: {
        'Content-Type': 'application/json'
    },
    timeout: 5000
});

axiosApi.interceptors.request.use((config) => {
    generalActionCreator.setLoadingStatus(true);

    return config;
}, (error) => {

    generalActionCreator.setLoadingStatus(false);

    messageService.showError("متاسفانه مشکلی رخ داده است.")
})
axiosApi.interceptors.response.use((response) => {

    generalActionCreator.setLoadingStatus(false);

    return response;
}, (error) => {
    
    if(error.response?.status === 401){
        authService.logout();
    }

    generalActionCreator.setLoadingStatus(false);

    messageService.showError("متاسفانه مشکلی رخ داده است.")
    return Promise.reject(error);
})

export default axiosApi
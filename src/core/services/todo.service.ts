import axiosApi from './../utils/Axios'
import { IApiCallbackModel } from '../models/api/api.models'
import todoActionCreators from './../../redux/actions/todo.action'
import userActionCreators from './../../redux/actions/user.action'
import userService from './user.service';


class TodoService {
    private _listLimitNumber : number = 10;
    private _getTodosListRequesttUrl = '/todos';
    private _getUserRequesttUrl = '/users';

    getTodosListRequest = () => {
        const hasTodoItems = userService.loadUsersData()?.todo?.todoItems;
        if(hasTodoItems) return;
        return axiosApi.get(this._getTodosListRequesttUrl + `?_limit=${this._listLimitNumber}`).then(response => {
            if (response.status) {
                let usersList = response.data.map(item => item.userId).filter((value, index, self) => self.indexOf(value) === index);
                response.data.map(item => item.listId = 1);
                usersList.forEach(userId => {
                    this.getUser(userId);
                });
                todoActionCreators.setTodoItems(response.data);
            }
        });
    }
    getUser = (userId: number, callback?: IApiCallbackModel) => {
        return axiosApi.get(this._getUserRequesttUrl + `/${userId}`).then(response => {
            if (response.status) {
                userActionCreators.addUser(response.data);
            }
        });
    }

}
const todoService = new TodoService();
export default todoService;
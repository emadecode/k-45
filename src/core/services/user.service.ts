class UserService {
    private _userDataKey = process.env.REACT_APP_USER_DATA!;

    hasData = () : boolean => {
        try {
            const serializedState = localStorage.getItem(this._userDataKey);
            if (serializedState === null) {
                return false;
            }
            return true;
        } catch (err) {
            return false;
        }
    };
    loadUsersData = () => {
        try {
            const serializedState = localStorage.getItem(this._userDataKey);
            if (serializedState === null) {
                return undefined;
            }
            return JSON.parse(serializedState);
        } catch (err) {
            return undefined;
        }
    };

    saveUserData = (state) => {
        try {
            const serializedState = JSON.stringify(state);
            localStorage.setItem(this._userDataKey, serializedState);
        } catch {
            // ignore write errors
        }
    };

}

export default new UserService();
import { toast } from 'react-toastify';

const options = {
    position: toast.POSITION.TOP_RIGHT,
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    pauseOnFocusLoss: false
}

const messageService = {
    showSuccess: (content) => {
        toast(content, { ...options, type: toast.TYPE.SUCCESS });
    },
    showError: (content) => {
        toast(content, { ...options, type: toast.TYPE.ERROR });
    },
    showInfo: (content) => {
        toast(content, { ...options, type: toast.TYPE.INFO });
    },
    showWarning: (content) => {
        toast(content, { ...options, type: toast.TYPE.WARNING });
    },
    showDark: (content) => {
        toast(content, { ...options, type: toast.TYPE.DARK });
    }

}

export default messageService;
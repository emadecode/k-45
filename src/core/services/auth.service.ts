import { IApiCallbackModel } from '../models/api/api.models'
import generalActionCreator from './../../redux/actions/general.action'
import { ILoginRequestRequestModel } from './../models/auth/auth.models';


class AuthService {

    private _tokenKey = process.env.REACT_APP_USER!;


    loginRequest = (model: ILoginRequestRequestModel, callback?: IApiCallbackModel) => {
        this.login(model.username);
        (typeof callback?.onSuccess === "function") && callback.onSuccess(model);
    }

    login = (token: string): void => {
        this.setToken(token);
    }
    logout = (): void => {
        localStorage.clear();
        generalActionCreator.userLogout();
    }
    setToken = (token: string): void => {
        localStorage.setItem(this._tokenKey, token);
    }
    getToken = (): string | null => {
        return localStorage.getItem(this._tokenKey);
    }
    isLogined = (): boolean => {
        return this.getToken() ? true : false;
    }
    getAuthName = () : string => {
        return localStorage.getItem(this._tokenKey);
    }
}

export default new AuthService();
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import authService from './../services/auth.service';

export const AuthGuard = ({ component: Component, ...rest }) => (

    <Route {...rest} render={(props: any) => (

        authService.isLogined() ? <Component {...props} /> : <Redirect to={
            {
                pathname: "/auth",
                state: {
                    returnUrl: props.location.pathname
                }
            }
        } />
    )} />

)
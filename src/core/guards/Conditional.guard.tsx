import React from 'react'
import { Route, Redirect } from 'react-router-dom';

const ConditionalGuard = ({ component: Component, check, ...rest }) => (
    <Route {...rest} render={(props: any) => check ? <Component {...props} /> : <Redirect to="/" />} />
)

export default ConditionalGuard

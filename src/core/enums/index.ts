export enum FormControlStatusEnum {
    SUCCESS = 'is-valid',
    FAIL = 'is-invalid',
    PENDING = 'is-pending'
}
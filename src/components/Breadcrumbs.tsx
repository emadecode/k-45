import React from 'react'
import { Link } from 'react-router-dom'
import { ArrowRight } from '@styled-icons/octicons/ArrowRight'
import { ThreeBars } from '@styled-icons/octicons/ThreeBars'
import generalActionCreator from './../redux/actions/general.action'

interface IPropsModel {
    title: string,
    subtitle: string,
    isRounded?: boolean,
    hideInDeskTop?: boolean,
    link?: string
}

const Breadcrumbs: React.FC<IPropsModel> = ({ title, subtitle, isRounded, hideInDeskTop, link, children }) => {
    return (
        <section className={`breadcrumb-section ${isRounded && "breadcrumb-section_is-rounded"}`} >
            <div className="container">
                <div className="row">
                    <div className="col-10 col-xl-12">
                        <div className="c-breadcrumb">
                            {link ?

                                <div className="c-breadcrumb__right">
                                    <Link to={link} className="c-breadcrumb__link">
                                        <ArrowRight width='3rem' />
                                    </Link>
                                </div>

                                :

                                <div className="c-breadcrumb__right d-block d-xl-none">
                                    <button type="button" className="c-breadcrumb__link" onClick={() => generalActionCreator.setMobileMenuStatus(true)}>
                                        <ThreeBars width="2.5rem" />
                                    </button>
                                </div>
                            }

                            <div className="c-breadcrumb__left">
                                <h3 className="c-breadcrumb__title">{title}</h3>
                                <h4 className="c-breadcrumb__subtitle">{subtitle}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col-2 d-xl-none text-right">
                        {children}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Breadcrumbs

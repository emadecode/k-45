import React, { FC, useEffect, useState } from 'react'
import { User } from '@styled-icons/boxicons-regular/User';
import { connect } from 'react-redux';

import { IUserModel } from '../core/models/user/user.models';
import authService from '../core/services/auth.service';
import { IAppState } from '../redux/reducers';

interface IPropsModel {
    userId: number | null,
    usersList?: Array<IUserModel>
}


const AuthorName: FC<IPropsModel> = ({ userId, usersList }) => {

    const [authName, setAuthName] = useState<string>("");

    useEffect(() => {
        if (userId === null) {
            setAuthName(authService.getAuthName())

        } else if (usersList.length) {
            setAuthName(usersList.find(item => item.id === userId).name);
        }
    }, [usersList, userId])

    return <h4 className="todo-author"><User className="todo-author__icon" />{authName}</h4>

}

const mapStatesToProps = (state: IAppState) => {
    return {
        usersList: state.user.usersList
    }
}

export default connect(mapStatesToProps)(AuthorName);

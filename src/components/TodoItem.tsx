import React, { FC, useState } from 'react'
import { Delete } from '@styled-icons/material/Delete'
import { Draggable } from 'react-beautiful-dnd'

import { ITodoItemModel, IUpdateTodoItemModel } from './../core/models/todo/todo.models'
import AuthorName from './AuthorName'
import todoActionCreators from './../redux/actions/todo.action';
import EditTodoForm from '../forms/EditTodo.form'

interface IPropsModel {
    todoItem: ITodoItemModel,
    index: number
}
const TodoItem: FC<IPropsModel> = ({ todoItem, index }) => {

    const [isCompleted, setIsCompleted] = useState<boolean>(todoItem.completed)

    const deleteTodoItem = () => {
        todoActionCreators.deleteTodoItem(todoItem.id)
    }

    const changeTodoStatus = () => {
        const model: IUpdateTodoItemModel = {
            ...todoItem,
            completed: !isCompleted
        }
        todoActionCreators.updateTodoItem(model);
        setIsCompleted(!isCompleted);
    }
    return (
        <Draggable draggableId={`${todoItem.id}`} index={index} id={todoItem.id}>
            {(provided) => (
                <div className={`todo-item ${isCompleted ? "todo-item--is-completed" : ""}`} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                    <div className="todo-item__header">
                        <AuthorName userId={todoItem.userId} />
                        <div className="todo-item__actions">
                            <button className="gi-btn todo-item-delete" onClick={() => deleteTodoItem()}><Delete width="20" /></button>
                        </div>
                    </div>
                    <div className="todo-item__body">
                        <EditTodoForm todo={todoItem} />
                    </div>
                    <div className="todo-item__footer">
                        <div className="todo-item-status" onClick={() => changeTodoStatus()}>
                            <input type="checkbox" className="todo-item-status__input" checked={isCompleted} readOnly />
                            <span className="">وضعیت: </span>
                            {isCompleted ?
                                <span className="">انجام شده</span>
                                :
                                <span className="">در حال انجام</span>
                            }
                        </div>
                    </div>

                </div>
            )}


        </Draggable>

    )
}

export default TodoItem

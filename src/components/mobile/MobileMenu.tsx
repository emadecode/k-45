import React, { FC, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux';
import { BorderStyle } from '@styled-icons/bootstrap'
import { CategoryAlt } from '@styled-icons/boxicons-solid/CategoryAlt'
import { PowerSettingsNew } from '@styled-icons/material-rounded'

import logo from './../../assets/images/logo/logo.png'
import { IAppState } from './../../redux/reducers/index';
import generalActionCreator from './../../redux/actions/general.action'

interface IPropsModel {
    isOpen: boolean
}

const MobileMenu: FC<IPropsModel> = ({ isOpen }) => {

    useEffect(() => {
        const bodyClassList = document.body.classList;
        isOpen ? bodyClassList.add("mobile-side-panel-open") : bodyClassList.remove("mobile-side-panel-open");
    }, [isOpen])

    const closeMobileMenu = () => {
        generalActionCreator.setMobileMenuStatus(false)
    }

    return (
        <div className={`mobile-side-panel ${isOpen && "mobile-side-panel_is-open"}`}>
            <div className="mobile-side-panel__overlay" onClick={closeMobileMenu}></div>
            <div className="mobile-side-panel__content">
                <div className="mobile-menu">
                    <div className="mobile-menu__header">
                        <div className="mobile-logo">
                            <img className="mobile-logo__image" src={logo} alt="logo" />
                        </div>
                    </div>
                    <div className="mobile-menu__body">
                        <nav className="mobile-navbar">
                            <ul className="mobile-navbar__list">
                                <li className="mobile-navbar__item" >
                                    <NavLink className="mobile-navbar__link" exact={true} to="/" onClick={closeMobileMenu}>
                                        <BorderStyle className="mobile-navbar__icon" /><span>پنل کاربری</span>
                                    </NavLink>
                                </li>
                                <li className="mobile-navbar__item">
                                    <NavLink className="mobile-navbar__link" to="/todo-list" onClick={closeMobileMenu}>
                                        <CategoryAlt className="mobile-navbar__icon" /><span>مدیریت لیست کارها</span>
                                    </NavLink>
                                </li>
                                <li className="mobile-navbar__item">
                                    <NavLink className="mobile-navbar__link" to="/logout" onClick={closeMobileMenu}>
                                        <PowerSettingsNew className="mobile-navbar__icon" /><span>خروج</span>
                                    </NavLink>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state: IAppState) => {
    return { isOpen: state.general.isMobileMenuOpen }
}

export default connect(mapStateToProps)(MobileMenu)

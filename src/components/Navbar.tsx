import React from 'react'
import { NavLink } from 'react-router-dom'
import { BorderStyle } from '@styled-icons/bootstrap'
import { PowerSettingsNew } from '@styled-icons/material-rounded'
import { CategoryAlt } from '@styled-icons/boxicons-solid/CategoryAlt'

const Navbar = () => {
    return (
        <nav className="main-menu">
            <ul className="main-menu__list">
                <li className="main-menu__item" >
                    <NavLink className="main-menu__link" exact={true} to="/">
                        <BorderStyle className="main-menu__icon" /><span>پنل کاربری</span>
                    </NavLink>
                </li>
                <li className="main-menu__item">
                    <NavLink className="main-menu__link" to="/todo-list">
                        <CategoryAlt className="main-menu__icon" /><span>مدیریت لیست کارها</span>
                    </NavLink>
                </li>
                <li className="main-menu__item">
                    <NavLink className="main-menu__link" to="/logout">
                        <PowerSettingsNew className="main-menu__icon" /><span>خروج</span>
                    </NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar

import React from 'react'
import { connect } from 'react-redux';

interface IPropsModel {
    showLoading: boolean
}

const Loading: React.FC<IPropsModel> = ({ showLoading }) => <div className={`loading ${showLoading && "loading_is-active"}`}></div>;

const mapStateToProps = state => {
    return { showLoading: state.general.showLoading }
}

export default connect(mapStateToProps)(Loading);

import React, { FC, useState } from 'react'
import { connect } from 'react-redux';
import { Edit } from '@styled-icons/boxicons-regular/Edit'
import { Delete } from '@styled-icons/material/Delete'
import { Droppable } from 'react-beautiful-dnd'

import { ITodoListModel } from '../core/models/todo/todo.models'
import { ITodoItemModel } from './../core/models/todo/todo.models'

import { IAppState } from '../redux/reducers'
import TodoItem from './TodoItem'
import AddTodoForm from './../forms/AddTodo.form'
import todoActionCreators from './../redux/actions/todo.action'
import Modal from 'react-bootstrap/esm/Modal'
import EditListForm from './../forms/EditList.form'

interface IPropsModel {
    todoList: ITodoListModel,
    todoItems?: Array<ITodoItemModel>
}

const TodoList: FC<IPropsModel> = ({ todoList, todoItems }) => {

    const [showAddTodoForm, setShowAddTodoForm] = useState<boolean>(false);
    const [showEditListModal, setShowEditListModal] = useState<boolean>(false);



    const renderItem = () => {
        if (!todoItems) return;
        return todoItems.filter(item => item.listId === todoList.id).map((item, index) => (
            <TodoItem todoItem={item} index={index} key={item.id} />
        ));
    }

    const deleteList = () => {
        todoActionCreators.deleteTodoList(todoList.id);
    }



    return (
        <div className="todo-list">
            <div className="todo-list__header">
                <h4 className="todo-list__title">{todoList.title}</h4>
                <div className="todo-list__actions">
                    <button className="gi-btn todo-list-edit" onClick={() => setShowEditListModal(true)}><Edit width="25" /></button>
                    <button className="gi-btn todo-list-delete" onClick={() => deleteList()}><Delete width="25" /></button>
                </div>
            </div>
            <div className="todo-list__body">
                
                {showAddTodoForm ?
                    <AddTodoForm listId={todoList.id} onCancelEvent={() => setShowAddTodoForm(false)} onSubmitEvent={() => setShowAddTodoForm(false)} />
                    :
                    <div className="text-center mb-2">
                        <button className="gi-btn gi-btn-block gi-btn-addTodo" onClick={() => setShowAddTodoForm(!showAddTodoForm)}>افزودن فعالیت جدید</button>
                    </div>
                }

                <Droppable droppableId={`${todoList.id}`}>
                    {(provided) => (
                        <div {...provided.droppableProps} ref={provided.innerRef}>
                            { renderItem()}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>


            </div>

            <Modal show={showEditListModal} centered={true} size="sm" onHide={() => setShowEditListModal(false)}>
                <Modal.Body>
                    <EditListForm todoList={todoList} onSubmitEvent={() => setShowEditListModal(false)} />
                </Modal.Body>
            </Modal>
        </div>
    )
}

const mapStatesToProps = (state: IAppState) => {
    return {
        todoItems: state.todo.todoItems
    }
}

export default connect(mapStatesToProps)(TodoList);
import React from 'react'
import buttonLoading from "./../../assets/images/animations/button-loading.svg"
import inputLoading from "./../../assets/images/animations/input-loading.svg"

type LoadingTypes = 'button' | 'input';

interface IPropsModel {
    check: any,
    width?: string,
    className?: string,
    loadingType?: LoadingTypes,
    children: any
}

const LoadingRender: React.FC<IPropsModel> = ({ children, check, className, width, loadingType = 'button' }) => {

    let loading;
    if (loadingType === 'button') loading = buttonLoading;
    if (loadingType === 'input') loading = inputLoading;

    return check ? <img className={`loading-render ${className ? className : ""}`} width={width} src={loading} alt="loading" /> : children;
}

export default LoadingRender


import React from 'react'
import Navbar from '../components/Navbar'
const Header = () => {


    return (
        <section className="header">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <Navbar />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Header

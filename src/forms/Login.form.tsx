import React, { FC, useState } from "react";
import { withRouter, RouteComponentProps} from "react-router-dom";
import { User } from '@styled-icons/boxicons-regular/User';


import { useForm } from "react-hook-form";
import { ILoginRequestRequestModel } from './../core/models/auth/auth.models';
import LoadingRender from "../components/utils/LoadingRender";
import authService from "../core/services/auth.service";
import customValidator from "../common/validators/custom.validators";


interface IFormInputs {
    username: string,
    password: string
}

interface IPropsModel {
    history: any
}

const LoginForm: FC<IPropsModel & RouteComponentProps> = ({ history }) => {

    const { register, handleSubmit, errors } = useForm<IFormInputs>();
    const [submitting, setSubmitting] = useState<boolean>(false);


    const onSubmit = (data: IFormInputs) => {
        setSubmitting(true);
        const model : ILoginRequestRequestModel = {
            username: data.username
        }

        authService.loginRequest(model, {
            onSuccess: () => { 
                let returnUrl = history.location.state?.returnUrl;
                history.push({ pathname: returnUrl || '/' });
                setSubmitting(false);
             }
        });
    }

    return (

        <div className="auth-form">
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="row">
                    <div className="col-12">
                        <h3 className="auth-form__title">ورود</h3>
                    </div>
                    <div className="col-12">
                        <div className="auth-form-group">
                            <div className="auth-input-group">
                                <input type="text" className="auth-input" placeholder="نام و نام خانوادگی" 
                                name="username" ref={register({ validate: (value) => customValidator.username(value) })} autoComplete="off" />
                                <span className="auth-icon">
                                    <User size='1.6rem' />
                                </span>
                            </div>
                            <div className="auth-hint">
                                <span>{errors.username && errors.username.message}</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 mb-5">
                        <button type="submit" className="auth-form-button" disabled={submitting}>
                            <LoadingRender check={submitting} width="24">ورود به سایت</LoadingRender>
                        </button>
                    </div>
                </div>
            </form>
        </div>

    );
}

export default withRouter(LoginForm);
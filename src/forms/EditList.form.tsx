import React, { FC, useState } from "react"
import Moment from 'react-moment'
import 'moment-timezone'
import { CalendarCheckFill } from "@styled-icons/bootstrap/CalendarCheckFill"
import { ClockFill } from "@styled-icons/bootstrap/ClockFill"
import { useForm } from "react-hook-form"
import LoadingRender from "../components/utils/LoadingRender"
import { ITodoListModel, IUpdateTodoListModel } from './../core/models/todo/todo.models'
import todoActionCreators from './../redux/actions/todo.action'

interface IPropsModel {
    todoList: ITodoListModel,
    onSubmitEvent: () => void
}
interface IFormInputs {
    title: string,
    description: string
}

const EditListForm: FC<IPropsModel> = ({ todoList, onSubmitEvent }) => {

    const { register, handleSubmit, errors } = useForm<IFormInputs>();
    const [submitting, setSubmitting] = useState<boolean>(false);

    const onSubmit = (data: IFormInputs) => {
        setSubmitting(true);
        const model: IUpdateTodoListModel = {
            id: todoList.id,
            title: data.title,
            description: data.description,
            createdAt: todoList.createdAt,
            updateAt: new Date().getTime()
        }

        todoActionCreators.updateTodoList(model);
        onSubmitEvent();
        setSubmitting(false);
    }

    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
                <div className="col-12 text-center ltr-input">
                    <div className="date-item">
                        <span className="date-item__box">
                            <CalendarCheckFill className="date-item__icon" />
                            <Moment format="YYYY/MM/DD" local tz="Asia/Tehran">
                                {todoList.createdAt}
                            </Moment>
                        </span>
                        <span className="date-item__box">
                            <ClockFill className="date-item__icon" />
                            <Moment format="HH:mm:ss" local tz="Asia/Tehran">
                                {todoList.createdAt}
                            </Moment>
                        </span>
                        <span className="date-item__box">ثبت</span>
                    </div>
                    <div className="date-item">
                        <span className="date-item__box">
                         
                            <CalendarCheckFill className="date-item__icon" />
                            <Moment format="YYYY/MM/DD" local tz="Asia/Tehran">
                                {todoList.updateAt}
                            </Moment>
                        </span>
                        <span className="date-item__box">
                            <ClockFill className="date-item__icon" />
                            <Moment format="HH:mm:ss" local tz="Asia/Tehran">
                                {todoList.updateAt}
                            </Moment>
                        </span>
                        <span className="date-item__box">ویرایش</span>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <input type="text" className="form-control" placeholder="عنوان لیست"
                            name="title" ref={register({ required: "عنوان لیست الزامی است!" })} autoComplete="off" defaultValue={todoList.title} />
                        <div className="auth-hint">
                            <span>{errors.title && errors.title.message}</span>
                        </div>
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" placeholder="توضیحات لیست"
                            name="description" ref={register({ required: "توضیحات لیست الزامی است!" })} defaultValue={todoList.description} ></textarea>
                        <div className="auth-hint">
                            <span>{errors.description && errors.description.message}</span>
                        </div>
                    </div>
                </div>
                <div className="col-12">
                    <button type="submit" className="gi-btn gi-btn-sumbit" disabled={submitting}>
                        <LoadingRender check={submitting} width="24">ثبت تغییرات</LoadingRender>
                    </button>
                </div>
            </div>
        </form>

    );
}

export default EditListForm;
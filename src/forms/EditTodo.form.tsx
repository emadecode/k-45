import React, { FC, useEffect, useState } from "react"
import { Edit } from '@styled-icons/boxicons-regular/Edit'
import { Cross } from '@styled-icons/entypo/Cross'

import { useForm } from "react-hook-form";
import { ITodoItemModel, IUpdateTodoItemModel } from './../core/models/todo/todo.models'
import todoActionCreators from './../redux/actions/todo.action'

interface IPropsModel {
    todo: ITodoItemModel,
}
interface IFormInputs {
    title: string
}

const EditTodoForm: FC<IPropsModel> = ({ todo }) => {

    const { register, handleSubmit, errors, control } = useForm<IFormInputs>();
    const [inputDisabled, setInputDisabled] = useState<boolean>(true);

    const onSubmit = (data: IFormInputs) => {
        const model: IUpdateTodoItemModel = {
            id: todo.id,
            title: data.title,
            listId: todo.listId,
            completed: todo.completed,
            userId: todo.userId
        }

        todoActionCreators.updateTodoItem(model);
        setInputDisabled(true);
    }

    useEffect(() => {
        if (!inputDisabled) {
            control.fieldsRef.current.title.ref.focus()
        }
    }, [inputDisabled, control.fieldsRef]);

    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <div className="todo-input-group">
                            <input type="text" className="form-control" placeholder="عنوان فعالیت"
                                name="title" ref={register({ required: "عنوان لیست الزامی است!" })} 
                                title={todo.title}
                                autoComplete="off" defaultValue={todo.title} disabled={inputDisabled} />
                            {inputDisabled
                                ?
                                <button type="button" className="gi-btn todo-item-edit" onClick={() => setInputDisabled(false)}><Edit width="20" /></button>
                                :
                                <button type="button" className="gi-btn todo-item-edit" onClick={() => setInputDisabled(true)}><Cross width="20" /></button>
                            }
                        </div>
                        <div className="auth-hint">
                            <span>{errors.title && errors.title.message}</span>
                        </div>
                    </div>
                </div>
            </div>
        </form >

    );
}

export default EditTodoForm;
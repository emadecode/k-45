import React, { FC, useState } from "react"
import { Cross } from '@styled-icons/entypo/Cross'

import { useForm } from "react-hook-form";
import LoadingRender from "../components/utils/LoadingRender"
import { IAddTodoItemModel } from './../core/models/todo/todo.models'
import todoActionCreators from './../redux/actions/todo.action'

interface IPropsModel {
    listId: number,
    onSubmitEvent: () => void,
    onCancelEvent: () => void
}
interface IFormInputs {
    title: string
}

const AddTodoForm: FC<IPropsModel> = ({listId, onSubmitEvent, onCancelEvent}) => {

    const { register, handleSubmit, errors } = useForm<IFormInputs>();
    const [submitting, setSubmitting] = useState<boolean>(false);


    const onSubmit = (data: IFormInputs) => {
        setSubmitting(true);
        const model: IAddTodoItemModel = {
            title: data.title,
            listId: listId,
            completed: false,
            userId: null
        }

        todoActionCreators.addTodoItem(model);
        onSubmitEvent();
        setSubmitting(false);
    }

    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <input type="text" className="form-control" placeholder="عنوان فعالیت"
                            name="title" ref={register({ required: "عنوان فعالیت الزامی است!" })} autoComplete="off" />
                        <div className="auth-hint">
                            <span>{errors.title && errors.title.message}</span>
                        </div>
                    </div>
                </div>
                <div className="col-9">
                    <button type="submit" className="gi-btn gi-btn-sumbit" disabled={submitting}>
                        <LoadingRender check={submitting} width="24">ثبت فعالیت جدید</LoadingRender>
                    </button>
                </div>
                <div className="col-3">
                    <button type="button" className="gi-btn gi-btn-cancel" onClick={() => onCancelEvent()}><Cross width="23" /></button>
                </div>
            </div>
        </form>

    );
}

export default AddTodoForm;
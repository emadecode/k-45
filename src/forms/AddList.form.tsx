import React, { FC, useState } from "react"

import { useForm } from "react-hook-form";
import LoadingRender from "../components/utils/LoadingRender"
import { IAddTodoListModel } from './../core/models/todo/todo.models'
import todoActionCreators from './../redux/actions/todo.action'


interface IFormInputs {
    title: string,
    description: string
}

const AddListForm: FC = () => {

    const { register, handleSubmit, errors } = useForm<IFormInputs>();
    const [submitting, setSubmitting] = useState<boolean>(false);


    const onSubmit = (data: IFormInputs) => {
        setSubmitting(true);
        const model: IAddTodoListModel = {
            title: data.title,
            description: data.description,
            createdAt: new Date().getTime(),
            updateAt: new Date().getTime()
        }

        todoActionCreators.addTodoList(model);
        setSubmitting(false);
    }

    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <input type="text" className="form-control" placeholder="عنوان لیست"
                            name="title" ref={register({ required: "عنوان لیست الزامی است!" })} autoComplete="off" />
                        <div className="auth-hint">
                            <span>{errors.title && errors.title.message}</span>
                        </div>
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" placeholder="توضیحات لیست"
                            name="description" ref={register({ required: "توضیحات لیست الزامی است!" })} ></textarea>
                        <div className="auth-hint">
                            <span>{errors.description && errors.description.message}</span>
                        </div>
                    </div>
                </div>
                <div className="col-12">
                    <button type="submit" className="gi-btn gi-btn-sumbit" disabled={submitting}>
                        <LoadingRender check={submitting} width="24">ثبت لیست جدید</LoadingRender>
                    </button>
                </div>
            </div>
        </form>

    );
}

export default AddListForm;